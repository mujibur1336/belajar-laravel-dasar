<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf        
        <label>First name</label><br><br>
        <input type="text" name="first_name"><br><br>
        <label>Last name</label><br><br>
        <input type="text" name="last_name"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="gender">Male <br><br>
        <input type="radio" name="gender">Female <br><br>
        <input type="radio" name="gender">Other <br><br>
        <label>Nationality</label><br><br>
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="indonesia">Bahasa Indonesia<br><br>
        <input type="checkbox" name="english">English<br><br>
        <input type="checkbox" name="other">Other<br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" cols="30" rows="5"></textarea><br><br>
        <input type="submit" value="submit">
    </form>
</body>

</html>