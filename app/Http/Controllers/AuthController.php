<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {        
        return view('form');
    }

    // public function welcome(Request $request)
    // {            
    //     // dd($request->["first_name"]);
    //     return "halo";
    // }
    public function welcome_post(Request $request)
    {    
        // dd($request->all());            
        $first_name = $request["first_name"];
        $last_name = $request["last_name"];
        return view('welcome', ["first_name" => $first_name,"last_name" => $last_name]);
    }
}
